# Icinga performance data loader and the charts web module

[![dashboard example](doc/dashboard_small.png)](doc/dashboard.png)

[![service view example](doc/service_small.png)](doc/service.png)

----

**Features**

* data stored in a normal SQL database; both Postgres and MySQL are supported,
  however the latter is not recommended due to inferior performance
* integrated Icinga Web module: no separate websites, no iframes
* SVG output customisable with CSS
* minimal dependencies, no external JS libraries
* mini-previews can be modified by host or service custom variables

# Installing

## Dependencies

Charts module:

* IcingaWeb with PHP 7+

Performace data loader:

* perl
* perl database modules: DBI, DBD::Pg or DBD::MariaDB

Debian: `apt install libdbd-pg-perl libdbd-mariadb-perl`

FreeBSD: `pkg install p5-DBD-Pg p5-DBD-MariaDB daemontools`

## Setting up the database

Postgres (assuming the final database user is "icinga"):

```shell
    psql --set user=icinga -f sql/perf-collector-pg.sql
```

MySQL:

```shell
    mysql --abort-source-on-error -ss < sql/perf-collector-mysql.sql
```

## Data hotfolder

Performance data loader reads files created by Icinga and stores their content
in the database. Icinga's configuration file defines the spool directory
**/var/spool/icinga2/icingaperf** which has to be created:

```shell
    mkdir -p /var/spool/icinga2/perf-collector/tmp
    mkdir -p /var/spool/icinga2/perf-collector/err
    chmod -R o= /var/spool/icinga2/perf-collector
```

Finally the configuration should be copied and enabled. For FreeBSD that's:

```shell
    cp etc/icinga2/features-available/perf-collector.conf \
    /usr/local/etc/icinga2/features-available
    ln -s ../features-available/perf-collector.conf \
    /usr/local/etc/icinga2/features-enabled
    chown -hR icinga /var/spool/icinga2/perf-collector
```

For Debian:

```shell
    cp etc/icinga2/features-available/perf-collector.conf \
    /etc/icinga2/features-available
    ln -s ../features-available/perf-collector.conf \
    /etc/icinga2/features-enabled
    chown -hR nagios.nagios /var/spool/icinga2/perf-collector
```

Restart Icinga. After it gathers some performance data from check plugins you
should see new files appearing in the spool directory.

## Data loader

Install the loader:

* FreeBSD:

```shell
    cp bin/perf-collector /usr/local/bin
    cp etc/rc.d/perfcollector /usr/local/etc/rc.d
```

* Debian

```shell
    cp bin/perf-collector /usr/local/bin
    cp etc/init.d/perfcollector /etc/init.d
```

Create the configuration file at:

* FreeBSD: **/usr/local/etc/icinga2/perf-collector.conf**
* Debian: **/etc/icinga2/perf-collector.conf**

containing the following lines:

    dbdriver = postgres
    host     = icinga.database.host
    username = icinga
    password = some-password
    dbname   = icinga
    log      = /var/log/icinga2/perf-collector.log
    pidfile  = /var/run/icinga2/perf-collector.pid

Database driver name is “postgres” or “maria” (for both MySQL and mariaDB). The
complete documentation is available at `perldoc bin/perf-collector`.

Now switch to Icinga's user and try to start the loader in foreground:

    perf-collector -FvvLstderr -c /usr/local/etc/icinga2/perf-collector.conf

or

    perf-collector -FvvLstderr -c /etc/icinga2/perf-collector.conf

If everything goes well it should start loading performance data into the
database. You may interrupt the program with Ctrl-C.

To make it start at boot:

* FreeBSD: add `perfcollector_enable="YES"` to /etc/rc.conf and execute
  `service perfcollector start`
* Debian (with rc scripts): 

```shell
    update-rc.d perfcollector defaults
    service perfcollector start
```

## IcingaWeb

Now the web module may be deployed. The content of the icingaweb2/modules
directory should be copied to where your icingaweb installation is.

FreeBSD:

```shell
    cp -r icingaweb2/modules /usr/local/www/icingaweb2
```

Debian:

```shell
    cp -r icingaweb2/modules /usr/share/icingaweb2
```

The module is called **charts** and should appear as an available extension
in IcingaWeb's configuration tab. After enabling it, open its main settings
and choose the IDO source pointing at the correct database. If you chose MySQL
as the database, then it is recommended to disable graph smoothing by entering
0 (zero) in the “Overlapping” box.

## Done

If the installation is successful you will now see miniature charts displayed
in host and service view. There is also a separate tab called “Graphs” with
full featured, configurable charts that can be pinned to the dashboard.

# Monitoring the loader itself

The package also comes with a monitoring plugin that reports perf-collector's
activity, queue length and whether there are any files in the error directory.

To deploy it on FreeBSD:

```shell
cp nagios/check_perfcollector /usr/local/libexec/nagios/
cp etc/icinga2/plugins.d/perfcollector.conf /usr/local/etc/icinga2/plugins.d/
```

On Debian that's:

```shell
cp nagios/check_perfcollector /usr/lib/nagios/plugins/
cp etc/icinga2/plugins.d/perfcollector.conf /etc/icinga2/conf.d/
```

Then you need to add the appropriate entry to _services.conf_. The following
configuration activates the plugin for local host:

```
apply Service "Perf Collector" {
    import "generic-service"
    check_command = "perfcollector"
    assign where host.name == NodeName
}
```

This enables basic checks for default directories. You may also specify where
perf-collector's configuration file is by adding this to local host's entry:

```
vars.perfcollector_config = "perf-collector.conf"
```

The following custom variables are available:

* perfcollector_warning – warning threshold for pending spool files
(default: 10),
* perfcollector_critical – critical threshold (default: 100),
* perfcollector_pid – path to the PID file where perf-collector stores its
process ID,
* perfcollector_dir – spool directory
(default: /var/spool/icinga2/perf-collector),
* perfcollector_error – error directory
(default: /var/spool/icinga2/perf-collector/err),
* perfcollector_config – perf-collector's configuration file; may be either
full path starting with / or relative to Icinga's config directory.

# Configuration

## General options

### IDO Backend

Allows to choose one of the configured database connections. Only SQL
databases are shown out of all available backends.

### Default graph length

This is the default time span for the mini charts in host and service view. The
default value is `6h`.

The general syntax of any duration parameters in this module is: number of days
suffixed with “d”, hours with “h”, minutes with “m”, and number of seconds. Any
of these components may be omitted, however they must appear in that order.

For example a duration of 2.5 hours may be written as `2h 30m`. Both bare
minutes (`150m`) and seconds (`9000`) are also correct.

### Maximum number of data points

Every line chart is made up of a limited number of points. Normally this
starts at the configured CI's check interval, meaning that is if a host or a
service is being queried on every minute, then the data points along the
horizontal axis will be aligned to exact one minute marks.

When the time span gets larger though, the points may actually be so dense,
that the chart becomes unreadable. When the computed number of data points
is greater than this value (default 1000), then the module starts reducing it
by averaging neighbouring probe results.

### Data overlapping

If a plugin execution happens to occur near exact time intervals, then the
chart may become chopped due to missing data. Suppose check interval is 1
minute, and Icinga schedules the execution very close to minute marks, then the
data timestamps may look like this: 9:11:01, 9:11:59, 9:13:00, 9:14:01, ... The
resulting chart would be discontinued because there is no data reading between
9:12:00 and 9:12:59.

Therefore the plugin actually extends the averaging window by the number of
seconds specified here. Given overlapping period of 10 seconds (the default
value) the data point for 9:12 from the example above would actually be
averaged over the period spanning from 9:11:50 until 9:13:10, data point
labeled as 9:13 would be an average from 9:12:50 to 9:14:10, and so on.

The default value is 10 seconds. Entering 0 disables this functionality.

### Probe search range

If disabled (set to 0) then only charts with any data found in the specified
range are displayed. Any graphs for which there was no plugin output in the
specified time span are excluded.

If enabled, with 3 being the default, then probe scanning is extended by the
given number of days before the computed start day. Also if there is no data
collected for a probe in during that time, the chart will still be displayed
as an empty box.

### Graph aspect ratio

Specifies how much wider a chart is compared to its height. Ratio of 1
produces square graphs. The higher this number is, the shorter the graphs are
(width is usually constrained by display dimensions).

The default value is 2.5.

### Always include zero

The vertical scale is normally adjusted between the highest and the lowest
visible data point. When this option is turned on (default is disabled)
vertical axes are going to start at zero.

# Per host and per service settings

The mini previews (and the default settings of the Graphs tab) can be
configured from within Icinga's main configuration for every monitored object.
This is done by setting
[custom variable](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#custom-variables)
called “charts”. For example:

    object Host "host1" {
        ...
        vars.charts = {
            zero   = 1
            length = "24h"
            probes = "rta"
        }
    }

The following settings may be overridden:

* *length* – changes the default graph time span,
* *zero* – enables or disables showing the zero point.

The *probes* value limits the number of charts that are displayed in the mini
preview. It may be either a string or an array of strings enumerating all the
charts that should be displayed. Wildcards “*” and “?” are also accepted.

# Author

matlib@matlibhax.com

Gitlab project page:
https://gitlab.com/Matlib/icingaperf
