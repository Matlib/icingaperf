--
-- perf-collector PostgreSQL upgrade script
-- from 0.2.x to 0.3.x
--
-- psql -f sql/upgrade-pg-0.3.sql
--

\set ECHO none
\set QUIET 1
\timing off
\set ON_ERROR_STOP 1

START TRANSACTION;

\echo Upgrading to v0.3
\echo
\echo - replacing unit with type TEXT

ALTER TABLE perf.hperf ALTER COLUMN unit TYPE TEXT;
ALTER TABLE perf.sperf ALTER COLUMN unit TYPE TEXT;
ALTER TABLE perf.hhrperf ALTER COLUMN unit TYPE TEXT;
ALTER TABLE perf.shrperf ALTER COLUMN unit TYPE TEXT;
ALTER TABLE perf.hdyperf ALTER COLUMN unit TYPE TEXT;
ALTER TABLE perf.sdyperf ALTER COLUMN unit TYPE TEXT;

COMMENT ON COLUMN perf.hperf.unit IS 'Unit of measurement';
COMMENT ON COLUMN perf.sperf.unit IS 'Unit of measurement';
COMMENT ON COLUMN perf.hhrperf.unit IS 'Unit of measurement';
COMMENT ON COLUMN perf.shrperf.unit IS 'Unit of measurement';
COMMENT ON COLUMN perf.hdyperf.unit IS 'Unit of measurement';
COMMENT ON COLUMN perf.sdyperf.unit IS 'Unit of measurement';

\echo Commiting

COMMIT;
