--
-- perf-collector MySQL installation script
--
-- mysql --abort-source-on-error -ss < perf-collector-mysql.sql
--

START TRANSACTION;

SELECT '';
SELECT 'Schema "perf"';

CREATE SCHEMA perf;

SELECT '- table "host"';

CREATE TABLE perf.host (
    host_id       INTEGER PRIMARY KEY,
    host_inserted DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    host_name     VARCHAR(256) UNIQUE NOT NULL
);

SELECT '- table "service"';

CREATE TABLE perf.service (
    service_id       INTEGER PRIMARY KEY,
    service_inserted DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    service_name     VARCHAR(256) UNIQUE NOT NULL
);

SELECT '- table "ccmd"';

CREATE TABLE perf.ccmd (
    ccmd_id       INTEGER PRIMARY KEY,
    ccmd_inserted DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    ccmd_name     VARCHAR(256) UNIQUE NOT NULL
);

SELECT '- table "probe"';

CREATE TABLE perf.probe (
    probe_id       INTEGER PRIMARY KEY,
    probe_inserted DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    probe_name     VARCHAR(256) UNIQUE NOT NULL
);

SELECT '- table "hdata"';

CREATE TABLE perf.hdata (
    hdata_id       BIGINT PRIMARY KEY AUTO_INCREMENT,
    hdata_inserted DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    hdata_ts       BIGINT NOT NULL,
    host_id        INTEGER NOT NULL,
    ccmd_id        INTEGER NOT NULL,
    hdata_state    INTEGER NOT NULL,
    hdata_hard     BOOLEAN NOT NULL
);

CREATE INDEX hdata_ts_idx ON perf.hdata (hdata_ts);

SELECT '- table "sdata"';

CREATE TABLE perf.sdata (
    sdata_id       BIGINT PRIMARY KEY AUTO_INCREMENT,
    sdata_inserted DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    sdata_ts       BIGINT NOT NULL,
    host_id        INTEGER NOT NULL,
    service_id     INTEGER NOT NULL,
    ccmd_id        INTEGER NOT NULL,
    sdata_state    INTEGER NOT NULL,
    sdata_hard     BOOLEAN NOT NULL
);

CREATE INDEX sdata_ts_idx ON perf.sdata (sdata_ts);

SELECT '- table "hperf"';

CREATE TABLE perf.hperf (
    hperf_id        BIGINT PRIMARY KEY AUTO_INCREMENT,
    hperf_inserted  DATETIME NOT NULL DEFAULT NOW(),
    hperf_ts        BIGINT NOT NULL,
    host_id         INTEGER NOT NULL,
    ccmd_id         INTEGER NOT NULL,
    probe_id        INTEGER NOT NULL,
    hperf_value     NUMERIC(29,10) NOT NULL,
    hperf_min       NUMERIC(29,10),
    hperf_max       NUMERIC(29,10),
    hperf_warning   NUMERIC(29,10),
    hperf_critical  NUMERIC(29,10),
    unit            VARCHAR(10)
);

CREATE INDEX hperf_ts_idx ON perf.hperf (hperf_ts);

SELECT '- table "sperf"';

CREATE TABLE perf.sperf (
    sperf_id        BIGINT PRIMARY KEY AUTO_INCREMENT,
    sperf_inserted  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    sperf_ts        BIGINT NOT NULL,
    host_id         INTEGER NOT NULL,
    service_id      INTEGER NOT NULL,
    ccmd_id         INTEGER NOT NULL,
    probe_id        INTEGER NOT NULL,
    sperf_value     NUMERIC(29,10) NOT NULL,
    sperf_min       NUMERIC(29,10),
    sperf_max       NUMERIC(29,10),
    sperf_warning   NUMERIC(29,10),
    sperf_critical  NUMERIC(29,10),
    unit            VARCHAR(10)
);

CREATE INDEX sperf_ts_idx ON perf.sperf (sperf_ts);

SELECT '- table "hhrperf"';

CREATE TABLE perf.hhrperf (
    hhrperf_id        BIGINT PRIMARY KEY AUTO_INCREMENT,
    hhrperf_inserted  DATETIME NOT NULL DEFAULT NOW(),
    hhrperf_ts        BIGINT NOT NULL,
    host_id           INTEGER NOT NULL,
    ccmd_id           INTEGER NOT NULL,
    probe_id          INTEGER NOT NULL,
    hhrperf_value     NUMERIC(29,10) NOT NULL,
    hhrperf_min       NUMERIC(29,10),
    hhrperf_max       NUMERIC(29,10),
    hhrperf_warning   NUMERIC(29,10),
    hhrperf_critical  NUMERIC(29,10),
    unit              VARCHAR(10)
);

CREATE INDEX hhrperf_ts_idx ON perf.hhrperf (hhrperf_ts);

SELECT '- table "shrperf"';

CREATE TABLE perf.shrperf (
    shrperf_id        BIGINT PRIMARY KEY AUTO_INCREMENT,
    shrperf_inserted  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    shrperf_ts        BIGINT NOT NULL,
    host_id           INTEGER NOT NULL,
    service_id        INTEGER NOT NULL,
    ccmd_id           INTEGER NOT NULL,
    probe_id          INTEGER NOT NULL,
    shrperf_value     NUMERIC(29,10) NOT NULL,
    shrperf_min       NUMERIC(29,10),
    shrperf_max       NUMERIC(29,10),
    shrperf_warning   NUMERIC(29,10),
    shrperf_critical  NUMERIC(29,10),
    unit              VARCHAR(10)
);

CREATE INDEX shrperf_ts_idx ON perf.shrperf (shrperf_ts);

SELECT '- table "hdyperf"';

CREATE TABLE perf.hdyperf (
    hdyperf_id        BIGINT PRIMARY KEY AUTO_INCREMENT,
    hdyperf_inserted  DATETIME NOT NULL DEFAULT NOW(),
    hdyperf_ts        BIGINT NOT NULL,
    host_id           INTEGER NOT NULL,
    ccmd_id           INTEGER NOT NULL,
    probe_id          INTEGER NOT NULL,
    hdyperf_value     NUMERIC(29,10) NOT NULL,
    hdyperf_min       NUMERIC(29,10),
    hdyperf_max       NUMERIC(29,10),
    hdyperf_warning   NUMERIC(29,10),
    hdyperf_critical  NUMERIC(29,10),
    unit              VARCHAR(10)
);

CREATE INDEX hdyperf_ts_idx ON perf.hdyperf (hdyperf_ts);

SELECT '- table "sdyperf"';

CREATE TABLE perf.sdyperf (
    sdyperf_id        BIGINT PRIMARY KEY AUTO_INCREMENT,
    sdyperf_inserted  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    sdyperf_ts        BIGINT NOT NULL,
    host_id           INTEGER NOT NULL,
    service_id        INTEGER NOT NULL,
    ccmd_id           INTEGER NOT NULL,
    probe_id          INTEGER NOT NULL,
    sdyperf_value     NUMERIC(29,10) NOT NULL,
    sdyperf_min       NUMERIC(29,10),
    sdyperf_max       NUMERIC(29,10),
    sdyperf_warning   NUMERIC(29,10),
    sdyperf_critical  NUMERIC(29,10),
    unit              VARCHAR(10)
);

CREATE INDEX sdyperf_ts_idx ON perf.sdyperf (sdyperf_ts);

SELECT '';
SELECT 'Giving permissions to user icinga';

SELECT '';
SELECT 'Commiting changes';

COMMIT;

SELECT '';
