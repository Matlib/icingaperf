--
-- perf-collector PostgreSQL installation script
--
-- psql --set user=icinga -f perf-collector-pg.sql
--

\set ECHO none
\set QUIET 1
\timing off
\set ON_ERROR_STOP 1

START TRANSACTION;

\echo
\echo Schema "perf":

CREATE SCHEMA perf;
COMMENT ON SCHEMA perf IS 'Performance data';

\echo - table "host"

CREATE TABLE perf.host (
    host_id       INTEGER PRIMARY KEY,
    host_inserted TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    host_name     TEXT UNIQUE NOT NULL
);

COMMENT ON TABLE perf.host IS 'Hosts';
COMMENT ON COLUMN perf.host.host_id IS 'Primary key';
COMMENT ON COLUMN perf.host.host_inserted IS 'When entry was added';
COMMENT ON COLUMN perf.host.host_name IS 'Host name';

\echo - table "service"

CREATE TABLE perf.service (
    service_id       INTEGER PRIMARY KEY,
    service_inserted TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    service_name     TEXT UNIQUE NOT NULL
);

COMMENT ON TABLE perf.service IS 'Services';
COMMENT ON COLUMN perf.service.service_id IS 'Primary key';
COMMENT ON COLUMN perf.service.service_inserted IS 'When entry was added';
COMMENT ON COLUMN perf.service.service_name IS 'Service name';

\echo - table "ccmd"

CREATE TABLE perf.ccmd (
    ccmd_id       INTEGER PRIMARY KEY,
    ccmd_inserted TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    ccmd_name     TEXT UNIQUE NOT NULL
);

COMMENT ON TABLE perf.ccmd IS 'Check commands';
COMMENT ON COLUMN perf.ccmd.ccmd_id IS 'Primary key';
COMMENT ON COLUMN perf.ccmd.ccmd_inserted IS 'When entry was added';
COMMENT ON COLUMN perf.ccmd.ccmd_name IS 'Check command name';

\echo - table "probe"

CREATE TABLE perf.probe (
    probe_id       INTEGER PRIMARY KEY,
    probe_inserted TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    probe_name     TEXT UNIQUE NOT NULL
);

COMMENT ON TABLE perf.probe IS 'Performance data labels';
COMMENT ON COLUMN perf.probe.probe_id IS 'Primary key';
COMMENT ON COLUMN perf.probe.probe_inserted IS 'When entry was added';
COMMENT ON COLUMN perf.probe.probe_name IS 'Performance data label';

\echo - sequence "data_seq"

CREATE SEQUENCE perf.data_seq;

\echo - table "hdata"

CREATE TABLE perf.hdata (
    hdata_id       BIGINT PRIMARY KEY DEFAULT NEXTVAL('perf.data_seq'),
    hdata_inserted TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    hdata_ts       BIGINT NOT NULL,
    host_id        INTEGER NOT NULL,
    ccmd_id        INTEGER NOT NULL,
    hdata_state    INTEGER NOT NULL,
    hdata_hard     BOOLEAN NOT NULL
);

CREATE INDEX ON perf.hdata USING BRIN (hdata_ts) WITH (AUTOSUMMARIZE);

COMMENT ON TABLE perf.hdata IS 'Host status data';
COMMENT ON COLUMN perf.hdata.hdata_id IS 'Primary key';
COMMENT ON COLUMN perf.hdata.hdata_inserted IS 'When entry was inserted';
COMMENT ON COLUMN perf.hdata.hdata_ts IS 'When host was probed';
COMMENT ON COLUMN perf.hdata.hdata_state IS 'Host state: 0 - UP, 1 - DOWN';
COMMENT ON COLUMN perf.hdata.hdata_hard IS 'Is state permament';

\echo - table "sdata"

CREATE TABLE perf.sdata (
    sdata_id       BIGINT PRIMARY KEY DEFAULT NEXTVAL('perf.data_seq'),
    sdata_inserted TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    sdata_ts       BIGINT NOT NULL,
    host_id        INTEGER NOT NULL,
    service_id     INTEGER NOT NULL,
    ccmd_id        INTEGER NOT NULL,
    sdata_state    INTEGER NOT NULL,
    sdata_hard     BOOLEAN NOT NULL
);

CREATE INDEX ON perf.sdata USING BRIN (sdata_ts) WITH (AUTOSUMMARIZE);

COMMENT ON TABLE perf.sdata IS 'Service status data';
COMMENT ON COLUMN perf.sdata.sdata_id IS 'Primary key';
COMMENT ON COLUMN perf.sdata.sdata_inserted IS 'When entry was inserted';
COMMENT ON COLUMN perf.sdata.sdata_ts IS 'When service was probed';
COMMENT ON COLUMN perf.sdata.sdata_state IS
    'Service state: 0 - OK, 1 - WARNING, 2 - CRITICAL, 3 - UNKNOWN';
COMMENT ON COLUMN perf.sdata.sdata_hard IS 'Is state permament';

\echo - table "hperf"

CREATE TABLE perf.hperf (
    hperf_id        BIGINT PRIMARY KEY DEFAULT NEXTVAL('perf.data_seq'),
    hperf_inserted  TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    hperf_ts        BIGINT NOT NULL,
    host_id         INTEGER NOT NULL,
    ccmd_id         INTEGER NOT NULL,
    probe_id        INTEGER NOT NULL,
    hperf_value     NUMERIC NOT NULL,
    hperf_min       NUMERIC,
    hperf_max       NUMERIC,
    hperf_warning   NUMERIC,
    hperf_critical  NUMERIC,
    unit            TEXT
);

CREATE INDEX ON perf.hperf USING BRIN (hperf_ts) WITH (AUTOSUMMARIZE);

COMMENT ON TABLE perf.hperf IS 'Host performance data';
COMMENT ON COLUMN perf.hperf.hperf_id IS 'Primary key';
COMMENT ON COLUMN perf.hperf.hperf_inserted IS 'When data was inserted';
COMMENT ON COLUMN perf.hperf.hperf_ts IS 'When performance data was received';
COMMENT ON COLUMN perf.hperf.hperf_value IS 'Value';
COMMENT ON COLUMN perf.hperf.hperf_min IS 'Expected minimum value';
COMMENT ON COLUMN perf.hperf.hperf_max IS 'Expected maximum value';
COMMENT ON COLUMN perf.hperf.hperf_warning IS 'Warning threshold';
COMMENT ON COLUMN perf.hperf.hperf_critical IS 'Critical threshold';
COMMENT ON COLUMN perf.hperf.unit IS 'Unit of measurement';

\echo - table "sperf"

CREATE TABLE perf.sperf (
    sperf_id        BIGINT PRIMARY KEY DEFAULT NEXTVAL('perf.data_seq'),
    sperf_inserted  TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    sperf_ts        BIGINT NOT NULL,
    host_id         INTEGER NOT NULL,
    service_id      INTEGER NOT NULL,
    ccmd_id         INTEGER NOT NULL,
    probe_id        INTEGER NOT NULL,
    sperf_value     NUMERIC NOT NULL,
    sperf_min       NUMERIC,
    sperf_max       NUMERIC,
    sperf_warning   NUMERIC,
    sperf_critical  NUMERIC,
    unit            TEXT
);

CREATE INDEX ON perf.sperf USING BRIN (sperf_ts) WITH (AUTOSUMMARIZE);

COMMENT ON TABLE perf.sperf IS 'Service performance data';
COMMENT ON COLUMN perf.sperf.sperf_id IS 'Primary key';
COMMENT ON COLUMN perf.sperf.sperf_inserted IS 'When data was inserted';
COMMENT ON COLUMN perf.sperf.sperf_ts IS 'When performance data was received';
COMMENT ON COLUMN perf.sperf.sperf_value IS 'Value';
COMMENT ON COLUMN perf.sperf.sperf_min IS 'Expected minimum value';
COMMENT ON COLUMN perf.sperf.sperf_max IS 'Expected maximum value';
COMMENT ON COLUMN perf.sperf.sperf_warning IS 'Warning threshold';
COMMENT ON COLUMN perf.sperf.sperf_critical IS 'Critical threshold';
COMMENT ON COLUMN perf.sperf.unit IS 'Unit of measurement';

\echo - table "hhrperf"

CREATE TABLE perf.hhrperf (
    hhrperf_id        BIGINT PRIMARY KEY DEFAULT NEXTVAL('perf.data_seq'),
    hhrperf_inserted  TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    hhrperf_ts        BIGINT NOT NULL,
    host_id           INTEGER NOT NULL,
    ccmd_id           INTEGER NOT NULL,
    probe_id          INTEGER NOT NULL,
    hhrperf_value     NUMERIC NOT NULL,
    hhrperf_min       NUMERIC,
    hhrperf_max       NUMERIC,
    hhrperf_warning   NUMERIC,
    hhrperf_critical  NUMERIC,
    unit              TEXT
);

CREATE INDEX ON perf.hhrperf USING BRIN (hhrperf_ts) WITH (AUTOSUMMARIZE);

COMMENT ON TABLE perf.hhrperf IS 'Host hourly performance data';
COMMENT ON COLUMN perf.hhrperf.hhrperf_id IS 'Primary key';
COMMENT ON COLUMN perf.hhrperf.hhrperf_inserted IS 'When data was inserted';
COMMENT ON COLUMN perf.hhrperf.hhrperf_ts IS 'When performance data was received';
COMMENT ON COLUMN perf.hhrperf.hhrperf_value IS 'Hourly average';
COMMENT ON COLUMN perf.hhrperf.hhrperf_min IS 'Expected hourly minimum value';
COMMENT ON COLUMN perf.hhrperf.hhrperf_max IS 'Expected hourly maximum value';
COMMENT ON COLUMN perf.hhrperf.hhrperf_warning IS
    'Hourly average warning threshold';
COMMENT ON COLUMN perf.hhrperf.hhrperf_critical IS
    'Hourly average critical threshold';
COMMENT ON COLUMN perf.hhrperf.unit IS 'Unit of measurement';

\echo - table "shrperf"

CREATE TABLE perf.shrperf (
    shrperf_id        BIGINT PRIMARY KEY DEFAULT NEXTVAL('perf.data_seq'),
    shrperf_inserted  TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    shrperf_ts        BIGINT NOT NULL,
    host_id           INTEGER NOT NULL,
    service_id        INTEGER NOT NULL,
    ccmd_id           INTEGER NOT NULL,
    probe_id          INTEGER NOT NULL,
    shrperf_value     NUMERIC NOT NULL,
    shrperf_min       NUMERIC,
    shrperf_max       NUMERIC,
    shrperf_warning   NUMERIC,
    shrperf_critical  NUMERIC,
    unit              TEXT
);

CREATE INDEX ON perf.shrperf USING BRIN (shrperf_ts) WITH (AUTOSUMMARIZE);

COMMENT ON TABLE perf.shrperf IS 'Service hourly performance data';
COMMENT ON COLUMN perf.shrperf.shrperf_id IS 'Primary key';
COMMENT ON COLUMN perf.shrperf.shrperf_inserted IS 'When data was inserted';
COMMENT ON COLUMN perf.shrperf.shrperf_ts IS 'When performance data was received';
COMMENT ON COLUMN perf.shrperf.shrperf_value IS 'Hourly average';
COMMENT ON COLUMN perf.shrperf.shrperf_min IS 'Expected hourly minimum value';
COMMENT ON COLUMN perf.shrperf.shrperf_max IS 'Expected hourly maximum value';
COMMENT ON COLUMN perf.shrperf.shrperf_warning IS
    'Hourly average warning threshold';
COMMENT ON COLUMN perf.shrperf.shrperf_critical IS
    'Hourly average critical threshold';
COMMENT ON COLUMN perf.shrperf.unit IS 'Unit of measurement';

\echo - table "hdyperf"

CREATE TABLE perf.hdyperf (
    hdyperf_id        BIGINT PRIMARY KEY DEFAULT NEXTVAL('perf.data_seq'),
    hdyperf_inserted  TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    hdyperf_ts        BIGINT NOT NULL,
    host_id           INTEGER NOT NULL,
    ccmd_id           INTEGER NOT NULL,
    probe_id          INTEGER NOT NULL,
    hdyperf_value     NUMERIC NOT NULL,
    hdyperf_min       NUMERIC,
    hdyperf_max       NUMERIC,
    hdyperf_warning   NUMERIC,
    hdyperf_critical  NUMERIC,
    unit              TEXT
);

CREATE INDEX ON perf.hdyperf USING BRIN (hdyperf_ts) WITH (AUTOSUMMARIZE);

COMMENT ON TABLE perf.hdyperf IS 'Host daily performance data';
COMMENT ON COLUMN perf.hdyperf.hdyperf_id IS 'Primary key';
COMMENT ON COLUMN perf.hdyperf.hdyperf_inserted IS 'When data was inserted';
COMMENT ON COLUMN perf.hdyperf.hdyperf_ts IS 'When performance data was received';
COMMENT ON COLUMN perf.hdyperf.hdyperf_value IS 'Daily average';
COMMENT ON COLUMN perf.hdyperf.hdyperf_min IS 'Expected daily minimum value';
COMMENT ON COLUMN perf.hdyperf.hdyperf_max IS 'Expected daily maximum value';
COMMENT ON COLUMN perf.hdyperf.hdyperf_warning IS
    'Daily average warning threshold';
COMMENT ON COLUMN perf.hdyperf.hdyperf_critical IS
    'Daily average critical threshold';
COMMENT ON COLUMN perf.hdyperf.unit IS 'Unit of measurement';

\echo - table "sdyperf"

CREATE TABLE perf.sdyperf (
    sdyperf_id        BIGINT PRIMARY KEY DEFAULT NEXTVAL('perf.data_seq'),
    sdyperf_inserted  TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    sdyperf_ts        BIGINT NOT NULL,
    host_id           INTEGER NOT NULL,
    service_id        INTEGER NOT NULL,
    ccmd_id           INTEGER NOT NULL,
    probe_id          INTEGER NOT NULL,
    sdyperf_value     NUMERIC NOT NULL,
    sdyperf_min       NUMERIC,
    sdyperf_max       NUMERIC,
    sdyperf_warning   NUMERIC,
    sdyperf_critical  NUMERIC,
    unit              TEXT
);

CREATE INDEX ON perf.sdyperf USING BRIN (sdyperf_ts) WITH (AUTOSUMMARIZE);

COMMENT ON TABLE perf.sdyperf IS 'Service daily performance data';
COMMENT ON COLUMN perf.sdyperf.sdyperf_id IS 'Primary key';
COMMENT ON COLUMN perf.sdyperf.sdyperf_inserted IS 'When data was inserted';
COMMENT ON COLUMN perf.sdyperf.sdyperf_ts IS 'When performance data was received';
COMMENT ON COLUMN perf.sdyperf.sdyperf_value IS 'Daily average';
COMMENT ON COLUMN perf.sdyperf.sdyperf_min IS 'Expected daily minimum value';
COMMENT ON COLUMN perf.sdyperf.sdyperf_max IS 'Expected daily maximum value';
COMMENT ON COLUMN perf.sdyperf.sdyperf_warning IS
    'Daily average warning threshold';
COMMENT ON COLUMN perf.sdyperf.sdyperf_critical IS
    'Daily average critical threshold';
COMMENT ON COLUMN perf.sdyperf.unit IS 'Unit of measurement';

\echo
\echo Giving permissions to user :user

GRANT USAGE ON SCHEMA perf TO :"user";
GRANT USAGE ON ALL SEQUENCES IN SCHEMA perf TO :"user";
GRANT SELECT, INSERT ON ALL TABLES IN SCHEMA perf TO :"user";
GRANT DELETE ON perf.hhrperf TO :"user";
GRANT DELETE ON perf.shrperf TO :"user";
GRANT DELETE ON perf.hdyperf TO :"user";
GRANT DELETE ON perf.sdyperf TO :"user";
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA perf TO :"user";

\echo
\echo Commiting changes

COMMIT;

\echo
