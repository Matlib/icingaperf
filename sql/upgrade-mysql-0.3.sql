--
-- perf-collector MySQL installation script
--
-- mysql --abort-source-on-error -ss < upgrade-mysql-0.3.sql
--

START TRANSACTION;

SELECT 'Upgrading to v0.3';
SELECT '';
SELECT '- replacing unit with type VARCHAR(10)';

ALTER TABLE perf.hperf MODIFY COLUMN unit VARCHAR(10);
ALTER TABLE perf.sperf MODIFY COLUMN unit VARCHAR(10);
ALTER TABLE perf.hhrperf MODIFY COLUMN unit VARCHAR(10);
ALTER TABLE perf.shrperf MODIFY COLUMN unit VARCHAR(10);
ALTER TABLE perf.hdyperf MODIFY COLUMN unit VARCHAR(10);
ALTER TABLE perf.sdyperf MODIFY COLUMN unit VARCHAR(10);

COMMIT;
