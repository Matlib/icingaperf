# 0.2.x to 0.3.x

Database format has changed to accomodate longer units of measurement. The old
monitoring plugin specification has only single letter units (s, %, B), while
now there are Ah, VA and others.

To upgrade Postgres database launch:

    psql -f sql/upgrade-pg-0.3.sql

To upgrade MySQL/mariaDB:

    mysql --abort-source-on-error -ss < sql/upgrade-mysql-0.3.sql
