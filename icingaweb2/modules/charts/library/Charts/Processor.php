<?php

namespace Icinga\Module\Charts;

use Icinga\Application\Config;
use Icinga\Exception\ConfigurationError;
use Icinga\Application\Hook\GrapherHook;
use Icinga\Module\Monitoring\Object\MonitoredObject;
use Icinga\Module\Monitoring\Object\Host;
use Icinga\Module\Monitoring\Object\Service;
use Icinga\Data\ResourceFactory;
use Icinga\Data\Db\DbConnection;

/**
 * The class responsible for retrieving data from the database and creating
 * common SVG elements.
 *
 * The performance data is stored in database tables called hperf (for hosts)
 * and sperf (services). They contain each check result and each individual
 * value (called “probe” throughout the module) returned from the monitored
 * object's plugin with exact time stamp.
 *
 * There are also hourly aggregate tables that hold average values for every
 * hour. They are recalculated periodically as needed by the loader script.
 *
 * This class first computes the correct graphing parameters depending on
 * the given start, end or time span, host's or service's check interval and
 * target maximum number of data points. Based on these factors it determines
 * the interval, that is the time difference between two resulting data points.
 *
 * The data point positions are aligned to the intervals regardless of the
 * requested starting point. If the calculated interval is 10 minutes, than
 * data points will be rounded down to nearest 10 minute marks as they occur
 * in UNIX timestamp.
 *
 * Then all the output value, warning and critical levels are averaged over
 * these periods of time. For interval of 10 minutes and X axis value of 10:30
 * the graph point represents the average between 10:30 and less than 10:40.
 *
 * If the interval is an hour or more, than the hourly tables are consulted.
 * For the default target number of data points of 1,000, this happens when the
 * time span is at least 42 days.
 *
 * If that interval is the same as monitored object's check interval than
 * overlapping is turned on. 
 *
 * The values of the variables controlling the outlook of the charts come from
 * four sources: form data (in the Graphs tab), monitored object's custom
 * variables, global configuration settings, and finally the built-in default
 * values.
 *
 * Per host and per service custom setting are stored in a variable called
 * charts, which has to be an object which in turn may contain the following
 * values:
 *
 * – probes – a string or an array of strings containing the names of the
 *   probes that are displayed in the preview; the strings may contain wildcard
 *   characters * and ?,
 *
 * – length – sets graphLength, that is the time span; the value is expressed
 *   with the syntax described at parseInterval(),
 *
 * – zero – sets graphZero.
 *
 * Example trivial configuration:
 *
 *     vars.charts = {
 *         probes = 'ada*'
 *         zero = 1
 *     }
 *
 */
class Processor
{

protected $db;
protected MonitoredObject $mobj;
protected $hostID;
protected $serviceID;

/**
 * Graph's time span expressed in the form that can be parsed with
 * parseInterval(). This value may be overruled if both graphStart and
 * graphStop are set.
 * @var string
 */
public $graphLength;

/**
 * Graph's start time (as timestamp). If left unset then the start point is
 * calculated from graphLength.
 * @var null|int
 */
public $graphStart;

/**
 * Graph's end time (as timestamp). The end point defaults to current time
 * unless graphStart is set.
 * @var null|int
 */
public $graphStop;

/**
 * The target maximum number of data points. The final number may exceed this
 * threshold depending on time span and time unit alignment.
 * @var int
 */
public $graphPoints;

/**
 * Graph glueing time value expressed as string that can be parsed with
 * parseInterval().
 * @var string
 */
public $graphOverlap;

/**
 * Number of days before the start date when the module looks for inactive
 * probes. Any older ones are not shown either as graphs or in the “Performance
 * data” form element.
 *
 * Zero disables showing any charts for which there is no data in the specified
 * time range, though the names collected during that day will still appear in
 * the select list.
 * @var string
 */
public $graphProbeSpan;

/**
 * The aspect ratio (width to height) of the resulting SVG image.
 * @var string
 */
public $graphAspect;

/**
 * Whether the vertical axis should always have zero point. If not set the
 * chart is scaled automatically depending on its visible minimum and maximum
 * values.
 * @var string
 */
public $graphZero;

/**
 * An array containing the default probe filters.
 * @var string
 */
public $defaultProbes;

/**
 * Initialize the object
 *
 * This method sets the default values for the configuration variables and
 * retrieves host_id and service_id from database for the monitored object
 * specified in the argument.
 */
public function init(MonitoredObject $object)
{
    $this->mobj = $object;
    $cfg = Config::module('charts')->getSection('backend');
    $dbc = new DbConnection (
        ResourceFactory::getResourceConfig( $cfg->get('ido') )
    );
    $this->db = $dbc->getDbAdapter();

    $data = $this->db->query("
        SELECT host_id
        FROM   perf.host
        WHERE  host_name = " . $this->db->quote($object->host_name) . "
    ")->fetchAll();
    if (count($data))
        $this->hostID = $data[0]->host_id;

    if ($object instanceof Host) {
        $vars = $object->hostVariables;
    }
    else {
        $data = $this->db->query("
            SELECT service_id
            FROM   perf.service
            WHERE  service_name = " . $this->db->quote($object->service) . "
        ")->fetchAll();
        if (count($data))
            $this->serviceID = $data[0]->service_id;
        else
            $this->hostID = null;
        $vars = $object->serviceVariables;
    }

    $cfg = Config::module('charts')->getSection('graph');
    $this->graphLength    = $cfg->get('length')  ?? '6h';
    $this->graphPoints    = $cfg->get('points')  ?? 1000;
    $this->graphOverlap   = $cfg->get('overlap') ?? '10s';
    $this->graphProbeSpan = $cfg->get('probespan') ?? '3';
    $this->graphAspect    = $cfg->get('aspect')  ?? 2.5;
    $this->graphZero      = $cfg->get('zero');

    if (!is_numeric($this->graphProbeSpan))
        throw new ConfigurationError(
            "Invalid probe scan interval '" . $this->graphProbeSpan . "'"
        );
    if (!is_numeric($this->graphAspect))
        throw new ConfigurationError(
            "Invalid aspect ratio '" . $this->graphAspect . "'"
        );
    $this->graphAspect *= 100;

    if (isset($vars) and isset($vars['charts']))
        $vars = $vars['charts'];
    else
        $vars = [];
    if (isset($vars->probes)) {
        if (is_array($vars->probes)) {
            $this->defaultProbes = $vars->probes;
        }
        else {
            $this->defaultProbes = [ $vars->probes ];
        }
        foreach ($this->defaultProbes as $p)
            if (!is_scalar($p))
                throw new ConfigurationError(
                    'Invalid probe filter array: '
                  . var_dump($this->defaultProbes)
                );
    }
    if (isset($vars->length)) {
        if ($this->parseInterval($vars->length))
            $this->graphLength = $vars->length;
    }
    if (isset($vars->zero))
        $this->graphZero = $vars->zero;
}

/**
 * Retrieve graph data from the database
 *
 * This method retrieves performance data from the database and creates the
 * actual SVG paths. It returns an array where indexes contain probe names, and
 * each value is an stdClass object with the following values:
 *
 * – count – the number of data points along the whole time span,
 *
 * – min – the lowest point in the graph,
 *
 * – max – the highest point,
 *
 * – unit – the unit of measurement of the values; if this value changes over
 *   time it will be set to the last one found,
 *
 * – start_ts – the start time,
 *
 * – stop_ts – the end time,
 *
 * – interval – time difference between subsequent data points,
 *
 * – path_open – the open path that draws the thick line on top of the graph,
 *
 * – path_closed – the closed path sements that fill the area under the graph,
 *
 * – path_warn – lines showing the warning levels recorded over time,
 *
 * – path_crit – lines showing the recorded critical levels.
 *
 * @return array
 */
public function get($probes = null)
{
    if (!isset($this->hostID))
        return [];

    list ( $start, $interval, $overlap, $points ) = $this->computeTiming();
    $aspect = $this->graphAspect;

    $probe_filter = '';
    if (isset($probes) and count($probes)) {
        foreach ($probes as $p) {
            $p = strtolower($p);
            $p = str_replace('\\', '\\\\', $p);
            $p = str_replace('%',  '\\%',  $p);
            $p = str_replace('_',  '\\_',  $p);
            $p = str_replace('*',  '%',    $p);
            $p = str_replace('?',  '_',    $p);
            $probe_filter .=
                ($probe_filter ? ' OR ' : '')
              . 'LOWER(probe_name)'
              . (
                    (strstr($p, '\\') or strstr($p, '%') or strstr($p, '_'))
                  ? ' LIKE '
                  : ' = '
                )
              . $this->db->quote($p);
        }
        $probe_filter =
            "AND probe_id IN "
          . "(SELECT probe_id FROM perf.probe WHERE $probe_filter)";
    }

    if (!$this->serviceID) {
        if ($interval >= 86400)
            $data = $this->fetchHost('dy', $start, $interval, $probe_filter);
        else if ($interval >= 3600)
            $data = $this->fetchHost('hr', $start, $interval, $probe_filter);
        else if ($overlap)
            $data = $this->fetchHostOverlap(
                $start, $interval, $overlap, $probe_filter
            );
        else
            $data = $this->fetchHost('', $start, $interval, $probe_filter);
    }
    else {
        if ($interval >= 86400)
            $data = $this->fetchService('dy', $start, $interval, $probe_filter);
        else if ($interval >= 3600)
            $data = $this->fetchService('hr', $start, $interval, $probe_filter);
        else if ($overlap)
            $data = $this->fetchServiceOverlap(
                $start, $interval, $overlap, $probe_filter
            );
        else
            $data = $this->fetchService('', $start, $interval, $probe_filter);
    }

    $probes = [];
    foreach ($data as $r) {
        if (!array_key_exists($r->probe_name, $probes)) {
            $probes[$r->probe_name] = new \stdClass();
            $probes[$r->probe_name]->count = $points;
            $probes[$r->probe_name]->min   = $this->graphZero ? 0 : $r->value;
            $probes[$r->probe_name]->max   = $this->graphZero ? 0 : $r->value;
            $probes[$r->probe_name]->unit  = $r->unit;
            $probes[$r->probe_name]->start_ts = $start;
            $probes[$r->probe_name]->stop_ts  = $start + $points * $interval;
            $probes[$r->probe_name]->interval = $interval;
        }
        if (!isset($probes[$r->probe_name]->min))
            $probes[$r->probe_name]->min = $r->value;
        if (!isset($probes[$r->probe_name]->max))
            $probes[$r->probe_name]->max = $r->value;
        if ($r->value != null and $probes[$r->probe_name]->min > $r->value)
            $probes[$r->probe_name]->min = $r->value;
        if ($r->value != null and $probes[$r->probe_name]->max < $r->value)
            $probes[$r->probe_name]->max = $r->value;
        if ($probes[$r->probe_name]->unit == null)
            $probes[$r->probe_name]->unit = $r->unit;
    }
    foreach (array_keys($probes) as $p) {
        if ($probes[$p]->max == $probes[$p]->min) {
            $dd =
              $probes[$p]->min ? floor( log( abs($probes[$p]->min), 10 ) ) : 0;
            if ($probes[$p]->min > 0) {
                $probes[$p]->min = 0;
                $probes[$p]->max += 10 ** $dd;
            }
            elseif ($probes[$p]->max < 0) {
                $probes[$p]->min -= 10 ** $dd;
                $probes[$p]->max = 0;
            }
            else {
                $probes[$p]->min = 0;
                $probes[$p]->max = 1;
            }
        }
        if (
            (abs($probes[$p]->min) / abs($probes[$p]->max - $probes[$p]->min))
            < .5
          )
        {
            if ($probes[$p]->min > 0)
                $probes[$p]->min = 0;
            elseif ($probes[$p]->max < 0)
                $probes[$p]->max = 0;
        }
        if ($probes[$p]->unit == null)
            $probes[$p]->unit = '';
    }

    if ($this->graphProbeSpan) {
        foreach ($this->getProbes($probe_filter) as $p) {
            if (array_key_exists($p, $probes))
                continue;
            $probes[$p] = new \stdClass();
            $probes[$p]->count = $points;
            $probes[$p]->min   = null;
            $probes[$p]->max   = null;
            $probes[$p]->unit  = '';
            $probes[$p]->start_ts = $start;
            $probes[$p]->stop_ts  = $start + $points * $interval;
            $probes[$p]->interval = $interval;
        }
    }

    uksort($probes, function ($a, $b) { return $this->vsort($a, $b); });

    foreach (array_keys($probes) as $p) {
        $pc = $po = $pwarn = $pcrit = '';
        $wx = $wv = $cx = $cv = $sx = $ex = null;
        $c = 0;

        $min = $probes[$p]->min;
        $max = $probes[$p]->max;
        if ($min >= 0 and $max >= 0)
            $zero = 100;
        elseif ($min < 0 and $max < 0)
            $zero = 0;
        else
            $zero = round(100 * ($max / ($max - $min)), 2);

        for (
            $dp = 0;
            $dp < count($data) and $data[$dp]->probe_name != $p;
            $dp++
        );

        $ts = $start;
        for ($c = 0; $c <= $points; $c++, $ts += $interval) {
            $r = null;
            if (
                    $dp < count($data)
                and $data[$dp]->probe_name == $p
                and $data[$dp]->tsb == $ts
              ) {
                $r = $data[$dp];
                $dp++;
            }
            $x = round($aspect * $c / $points, 2);
            if (isset($r->warn) and $wv != $r->warn) {
                if (isset($wv)) {
                    $y = round(100 - 100 * ($wv - $min) / ($max - $min), 2);
                    $pwarn .= " M $wx $y L $x $y";
                    $wv = $r->warn;
                    $wx = $x;
                }
                else {
                    $wv = $r->warn;
                    $wx = 0;
                }
            }
            if (isset($r->crit) and $cv != $r->crit) {
                if (isset($cv)) {
                    $y = round(100 - 100 * ($cv - $min) / ($max - $min), 2);
                    $pcrit .= " M $cx $y L $x $y";
                    $cv = $r->crit;
                    $cx = $x;
                }
                else {
                    $cv = $r->crit;
                    $cx = 0;
                }
            }
            if (!isset($r->value)) {
                if (isset($sx)) {
                    $pc .= " L $ex $zero L $sx $zero Z";
                    $ex = $sx = null;
                }
                continue;
            }
            $y = round(100 - 100 * ($r->value - $min) / ($max - $min), 2);
            if (isset($sx)) {
                $pc .= " L $x $y";
                $po .= " L $x $y";
            }
            else {
                $xh = round($aspect * ($c + .1) / $points, 2);
                $pc .= " M $x $y L $xh $y";
                $po .= " M $x $y L $xh $y";
                $sx = $x;
            }
            $ex = $x;
        }
        if (isset($sx))
            $pc .= " L $ex $zero L $sx $zero Z";
        if (isset($wx)) {
            $y = round(100 - 100 * ($wv - $min) / ($max - $min), 2);
            $pwarn .= " M $wx $y L $aspect $y";
        }
        if (isset($cx)) {
            $y = round(100 - 100 * ($cv - $min) / ($max - $min), 2);
            $pcrit .= " M $cx $y L $aspect $y";
        }

        $probes[$p]->path_open   = $po;
        $probes[$p]->path_closed = $pc;
        $probes[$p]->path_warn   = $pwarn;
        $probes[$p]->path_crit   = $pcrit;
        $probes[$p]->zero        = $zero;
    }

    return $probes;
}

/**
 * Returns the probes found in the data over the defined time span
 *
 * This method looks for any probes found in the hourly tables and returns
 * the result as a sorted array of strings. Accepts one optional argument –
 * SQL expression used for probe filtering.
 *
 * @return array
 */
public function getProbes($probe_filter = '')
{
    if (!isset($this->hostID))
        return [];

    list ( $start, $interval, $overlap, $points ) = $this->computeTiming();
    $start = floor($start / 86400) * 86400 - 86400 * $this->graphProbeSpan;

    if (!$this->serviceID) {
        $data = $this->db->query("
            SELECT
                probe_name
            FROM perf.probe
            WHERE
                probe_id IN (
                    SELECT DISTINCT
                        probe_id
                    FROM perf.hdyperf
                    WHERE
                        " . $this->tLimit('hdyperf_ts', $start, $interval) . "
                      AND
                        host_id = $this->hostID
                      $probe_filter
                )
            ORDER BY probe_name
        ")->fetchAll();
    }

    else {
        $data = $this->db->query("
            SELECT
                probe_name
            FROM perf.probe
            WHERE
                probe_id IN (
                    SELECT DISTINCT
                        probe_id
                    FROM perf.sdyperf
                    WHERE
                        " . $this->tLimit('sdyperf_ts', $start, $interval) . "
                      AND
                        host_id = $this->hostID
                      AND
                        service_id = $this->serviceID
                      $probe_filter
                )
            ORDER BY probe_name
        ")->fetchAll();
    }

    $probes = [];
    foreach ($data as $r)
        array_push($probes, $r->probe_name);
    usort($probes, function ($a, $b) { return $this->vsort($a, $b); });
    return $probes;
}

/**
 * Calculate the timing values
 *
 * This method calculates the timing characteristics of the graph depending on
 * the requested start and stop times, duration, target data points, and
 * object's own check interval.
 *
 * First it determines the start and the time span depending on whether
 * graphStart, graphStop and graphLength are set. The length is ignored if both
 * start and stop are set. If only the length is given then stop defaults to
 * current time.
 *
 * The interval – that is the time distance between two adjacent data points –
 * defaults to monitored object's check interval. However if that exceeds
 * graphPoints, the interval is doubled until it the number of data points is
 * less than graphPoints, and the overlap value is reset to zero.
 *
 * Finally, the calculated start time is rounded down to the nearest interval
 * unit and the number of data points is rounded the the nearest integer.
 *
 * It returns an array with the following values: start timestamp, interval,
 * overlap, and the number of data points.
 *
 * @return array
 */
public function computeTiming()
{
    if (
            isset($this->graphStart) and isset($this->graphStop)
        and $this->graphStop > $this->graphStart
      )
        $time_span = $this->graphStop - $this->graphStart;
    else if (isset($this->graphStart) and $this->graphStart < time())
        $time_span = time() - $this->graphStart;
    else
        $time_span = $this->parseInterval($this->graphLength);
    $overlap   = $this->parseInterval($this->graphOverlap);
    $interval  =
        ($this->mobj instanceof Host)
      ? $this->mobj->host_check_interval
      : $this->mobj->service_check_interval;
    $points = $time_span / $interval;
    while ($points > $this->graphPoints) {
        $interval *= 2;
        $points /= 2;
        $overlap = 0;
    }
    if ($interval >= 3600) {
        $interval = round($interval / 3600) * 3600;
        $points = $time_span / $interval;
    }
    $points = round($points);

    $start = isset($this->graphStop) ? $this->graphStop : time();
    $start = floor($start / $interval)  * $interval - $points * $interval;

    return [ $start, $interval, $overlap, $points ];
}

/**
 * Returns the SQL condition for data retrieval
 *
 * This method takes three arguments: the name of the timestamp column in the
 * source table, the requested start time and interval. Start and interval
 * are normally returned by computeTiming(). If graphStop is set then the
 * condition requests the timestamp column fit within the start and graphStop
 * plus one interval. Otherwise the timestamp must be greater or equal the
 * start time.
 *
 * @return string
 */
private function tLimit($tscol, $start, $interval)
{
    return
        isset($this->graphStop)
      ? "$tscol BETWEEN $start AND $this->graphStop + $interval"
      : "$tscol >= $start";
}

/**
 * Returns the end timestamp
 *
 * This method returns graphStop if it is set, and current time minus one
 * interval otherwise.
 *
 * @return int
 */
private function sLimit($interval)
{
    return $this->graphStop ?? (time() - $interval);
}

/**
 * Fetch data for host without overlapping
 *
 * This method retrieves performance data from database averaged over exact
 * intervals. It takes four arguments: aggregate type, the start time,
 * interval, and SQL condition for filtering requested probes by their
 * probe_id.
 *
 * The aggregate type tells which source table is used:
 *
 * • '' (empty string) – raw host performance table,
 * • 'hr' – hourly aggregate,
 * • 'dy' – daily aggregate.
 *
 * The following columns are returned: textual probe name, data point
 * timestamp, average value over the given interval starting from the
 * timestamp, average warning and critical levels, and one of the units of
 * measure that appeared within the interval.
 *
 * @return array
 */
private function fetchHost($type, $start, $interval, $probe_filter)
{
    return $this->db->query("
        WITH
            dps AS (
                SELECT
                    probe_id,
                    h${type}perf_ts - MOD(h${type}perf_ts, $interval) AS tsb,
                    AVG(h${type}perf_value)    AS value,
                    AVG(h${type}perf_warning)  AS warn,
                    AVG(h${type}perf_critical) AS crit,
                    MIN(unit) AS unit
                FROM perf.h${type}perf
                WHERE
                    " . $this->tLimit("h${type}perf_ts", $start, $interval) . "
                  AND
                    host_id = $this->hostID
                  $probe_filter
                GROUP BY probe_id, tsb
            )
        SELECT
            probe_name,
            tsb,
            value,
            warn,
            crit,
            unit
        FROM dps
        JOIN perf.probe USING (probe_id)
        ORDER BY probe_name, tsb
    ")->fetchAll();
}

/**
 * Fetch data for host with overlapping
 *
 * This method retrieves performance data from database averaged over intervals
 * increased by overlap in both directions. It takes four arguments: the
 * start time, interval, overlap time, and SQL condition for filtering
 * requested probes by their probe_id.
 *
 * It returns the same columns as fetchHost().
 *
 * @return array
 */
private function fetchHostOverlap($start, $interval, $overlap, $probe_filter)
{
    return $this->db->query("
        WITH
            RECURSIVE tseries(tsb, tsl, tsh) AS (
                SELECT
                    $start,
                    $start - $overlap,
                    $start + $interval + $overlap
                UNION ALL
                SELECT
                    tsb + $interval,
                    tsb + $interval - $overlap,
                    tsb + $interval + $interval + $overlap
                FROM tseries
                WHERE
                    tsb <= " . $this->sLimit($interval) . "
            ),
            data AS (
                SELECT
                    probe_id,
                    hperf_ts AS ts,
                    hperf_value,
                    hperf_warning,
                    hperf_critical,
                    unit
                FROM perf.hperf
                WHERE
                    " . $this->tLimit('hperf_ts', $start, $interval) . "
                  AND
                    host_id = $this->hostID
                  $probe_filter
            ),
            dps AS (
                SELECT
                    probe_id,
                    tsb,
                    AVG(hperf_value)    AS value,
                    AVG(hperf_warning)  AS warn,
                    AVG(hperf_critical) AS crit,
                    MIN(unit)           AS unit
                FROM tseries
                JOIN data ON ts >= tsl AND ts <= tsh
                GROUP BY probe_id, tsb
            )
        SELECT
            probe_name,
            tsb,
            value,
            warn,
            crit,
            unit
        FROM dps
        JOIN perf.probe USING (probe_id)
        ORDER BY probe_name, tsb
    ")->fetchAll();
}

/**
 * Fetch data for service without overlapping
 *
 * This method is a service counterpart to fetchHost(). It accepts the same
 * arguments and returns the same output columns.
 *
 * @return array
 */
private function fetchService($type, $start, $interval, $probe_filter)
{
    return $this->db->query("
        WITH
            dps AS (
                SELECT
                    probe_id,
                    s${type}perf_ts - MOD(s${type}perf_ts, $interval) AS tsb,
                    AVG(s${type}perf_value)    AS value,
                    AVG(s${type}perf_warning)  AS warn,
                    AVG(s${type}perf_critical) AS crit,
                    MIN(unit) AS unit
                FROM perf.s${type}perf
                WHERE
                    " . $this->tLimit("s${type}perf_ts", $start, $interval) . "
                  AND
                    host_id = $this->hostID
                  AND
                    service_id = $this->serviceID
                  $probe_filter
                GROUP BY probe_id, tsb
            )
        SELECT
            probe_name,
            tsb,
            value,
            warn,
            crit,
            unit
        FROM dps
        JOIN perf.probe USING (probe_id)
        ORDER BY probe_name, tsb
    ")->fetchAll();
}

/**
 * Fetch data for service with overlapping
 *
 * This method is called when the monitored object is a service. The arguments
 * and the output are the same as for fetchHostOverlap().
 *
 * @return array
 */
private function fetchServiceOverlap($start, $interval,
    $overlap, $probe_filter)
{
    return $this->db->query("
        WITH
            RECURSIVE tseries(tsb, tsl, tsh) AS (
                SELECT
                    $start,
                    $start - $overlap,
                    $start + $interval + $overlap
                UNION ALL
                SELECT
                    tsb + $interval,
                    tsb + $interval - $overlap,
                    tsb + $interval + $interval + $overlap
                FROM tseries
                WHERE
                    tsb <= " . $this->sLimit($interval) . "
            ),
            data AS (
                SELECT
                    probe_id,
                    sperf_ts AS ts,
                    sperf_value,
                    sperf_warning,
                    sperf_critical,
                    unit
                FROM perf.sperf
                WHERE
                    " . $this->tLimit('sperf_ts', $start, $interval) . "
                  AND
                    host_id = $this->hostID
                  AND
                    service_id = $this->serviceID
                  $probe_filter
            ),
            dps AS (
                SELECT
                    probe_id,
                    tsb,
                    AVG(sperf_value)    AS value,
                    AVG(sperf_warning)  AS warn,
                    AVG(sperf_critical) AS crit,
                    MIN(unit)           AS unit
                FROM tseries
                JOIN data ON ts >= tsl AND ts <= tsh
                GROUP BY probe_id, tsb
            )
        SELECT
            probe_name,
            tsb,
            value,
            warn,
            crit,
            unit
        FROM dps
        JOIN perf.probe USING (probe_id)
        ORDER BY probe_name, tsb
    ")->fetchAll();
}

/**
 * Format the given number along with its unit of measure
 *
 * This method accepts three arguments: a numeric value, unit of measure and
 * the optional requested decimal precision. Returns a formatted string that
 * depends on the unit and precision. For seconds it just calls formatAge(),
 * and for bytes it calls formatBytes().
 *
 * Otherwise it moves the decimal point by three until it gets a number
 * somewhere between 0.1 and 1000 and adds the appropriate suffix – p, n, and
 * so on until T. The unit is appended at the end too.
 *
 * @return string
 */
public function formatNumber($n, $unit, $dd = null)
{
    if ($n == 0)
        return 0;
    if ($unit == 's')
        return $this->formatAge($n);
    if ($unit == 'B')
        return $this->formatBytes($n, $dd);

    $neg = $n < 0 ? '-' : '';
    $n = abs($n);

    if (!isset($dd))
        $dd = 1;
    if ($n >= .1 && $n < 1000)
        return sprintf("$neg%.${dd}f%s", $n, $unit);

    if ($n < 1) {
        foreach ([ 'm', 'µ', 'n', 'p' ] as $p) {
            $n *= 1000;
            if ($n > .1)
                return sprintf("$neg%.${dd}f$p%s", $n, $unit);
        }
        return sprintf("$neg%.${dd}f", $n) . 'p' . $unit;
    }

    foreach ([ 'k', 'M', 'G', 'T' ] as $p) {
        $n /= 1000;
        if ($n < 1000)
            return sprintf("$neg%.${dd}f$p%s", $n, $unit);
    }
    return sprintf("$neg%.${dd}fT%s", $n, $unit);
}

/**
 * Format time
 *
 * This method accepts time in seconds as its argument. It returns time
 * formatted by extracting full days, hours and minutes. Seconds are rounded to
 * one decimal point and the trailing zero is removed.
 *
 * @return string
 */
public function formatAge($a)
{
    if ($a < 1)
        return $this->formatNumber($a, '') . 's';
    $d = floor($a / 86400);
    $h = floor((floor($a) % 86400) / 3600);
    $m = floor((floor($a) % 3600) / 60);
    $s = preg_replace('/\.0/', '', round(floor($a) % 60, 1));
    return preg_replace(
        '/ $/', '',
        ($d ? "${d}d " : "")
      . ($h ? "${h}h " : "")
      . ($m ? "${m}m " : "")
      . ($s ? $s       : "")
    );
}

/**
 * Format bytes
 *
 * This method takes two arguments: a number of bytes, and precision. It
 * divides the number by powers of 1024 until it gets a value greater than 1,
 * and adds the appropriate suffix.
 *
 * It uses the same dumb ISO suffixes as the monitoring module to keep
 * consistent number formatting, so I had to code “Ti” for terabytes, “Gi” for
 * gigabytes, “Mi” for megabytes, and “Ki” for kilobytes.
 *
 * @return string
 */
public function formatBytes($b, $dd = null)
{
    $b = abs($b);
    $d = 1099511627776;
    foreach ([ 'Ti', 'Gi', 'Mi', 'Ki' ] as $s) {
        if ($b >= $d)
            return 
                isset($dd)
              ? sprintf("%.${dd}f%sB", $b / $d, $s)
              : preg_replace('/\.0/', '', round($b / $d, 1)) . $s . 'B';
        $d /= 1024;
    }
    return $b;
}

/**
 * Parse a time string
 *
 * This method parses a string from the argument and converts it to a number of
 * seconds. The general format is: number of days suffixed by “d”, number of
 * hours with “h”, number of minutes with “m” and number of seconds with
 * optional “s”. Not all of these parts are required, however they must appear
 * in that order.
 *
 * It throws ConfigurationError if the string is invalid.
 *
 * @return int
 */
public function parseInterval($interval)
{
    if ($interval == null || preg_match('/^\s*$/s', $interval))
        throw new ConfigurationError("Empty interval");
    if (
        !preg_match(
            '/^ *(?:([1-9][0-9]{0,9}) *(?:d|days?))? *'
          . '(?:([1-9][0-9]{0,9}) *(?:h|hours?))? *'
          . '(?:([1-9][0-9]{0,9}) *(?:m|min|minutes?))? *'
          . '(?:([0-9]{0,10}) *(?:|s|sec|seconds?))? *$/i',
            $interval,
            $matches
        )
      )
        throw new ConfigurationError("Invalid interval: '$interval'");
    $d = (array_key_exists(1, $matches) and $matches[1]) ? $matches[1] : 0;
    $h = (array_key_exists(2, $matches) and $matches[2]) ? $matches[2] : 0;
    $m = (array_key_exists(3, $matches) and $matches[3]) ? $matches[3] : 0;
    $s = (array_key_exists(4, $matches) and $matches[4]) ? $matches[4] : 0;
    return $s + $m * 60 + $h * 3600 + $d * 86400;
}

/**
 * Parse date and time string
 *
 * This method parses a string as returned by DOM INPUT element with type
 * datetime. The general format is YYYY-MM-DDTHH:MM:SS, however the time
 * as well as the trailing zeroes of the time may be ommited. Only the full
 * and correct date is obligatory.
 *
 * Returns the timestamp corresponding to the given date and/or time.
 *
 * @return int
 */
public function parseDateTime($dt)
{
    if (
        !preg_match(
            '/^ *([0-9]+)-([0-9]+)-([0-9]+)'
          . '[ T]+'
          . '(?:([0-9]+)(?::([0-9]+)(?::([0-9]+))?)?)? *$/i',
            $dt,
            $m
        )
      )
        return null;
    return mktime(
        $m[4] ?? 0,
        $m[5] ?? 0,
        $m[6] ?? 0,
        $m[2],
        $m[3],
        $m[1]
    );
}

/**
 * Version sort
 *
 * This method implements the version sort comparison function similar to GNU's
 * strverscmp(). String parts are compared as strings, while any consecutive
 * digits not including leading zeroes are merged and compared as numbers.
 *
 * Accepts two strings as arguments and returns 0 if strings are exactly the
 * same, -1 if the first argument precedes the second, and 1 otherwise.
 *
 * @return int
 */
public function vsort($a, $b)
{
    $pa = $pb = 0;
    while ($pa < strlen($a) and $pb < strlen($b)) {
        $ca = substr($a, $pa, 1);
        $cb = substr($b, $pb, 1);
        if (is_numeric($ca) and is_numeric($cb) and $ca > 0 and $cb > 0) {
            $na = $ca;
            $nb = $cb;
            while (++$pa < strlen($a) and is_numeric($ca = substr($a, $pa, 1)))
                $na .= $ca;
            while (++$pb < strlen($b) and is_numeric($cb = substr($b, $pb, 1)))
                $nb .= $cb;
            if ($na < $nb)
                return -1;
            if ($na > $nb)
                return 1;
            continue;
        }
        if (strcasecmp($ca, $cb))
            return strcasecmp($ca, $cb);
        $pa++;
        $pb++;
    }
    if ($pb < strlen($b))
        return -1;
    if ($pa < strlen($a))
        return 1;
    return 0;
}

}
