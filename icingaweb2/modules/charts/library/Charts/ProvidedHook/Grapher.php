<?php

namespace Icinga\Module\Charts\ProvidedHook;

use Icinga\Module\Charts\Processor;

use Icinga\Application\Hook\GrapherHook;
use Icinga\Module\Monitoring\Object\MonitoredObject;
use Icinga\Module\Monitoring\Object\Host;
use Icinga\Module\Monitoring\Object\Service;
use Icinga\Web\Url;

/**
 * Grapher hook that is responsible for displaying mini-previews in host or
 * service tab. It shows small graphs with minimum and maximum values one next
 * to another.
 *
 * The previews may be customized for each individual monitored object using
 * its custom variables. The variables are described in the Processor module.
 */
class Grapher extends GrapherHook
{

/**
 * Check if the charts are available
 *
 * Returns the state of object's process_perfdata variable.
 *
 * return bool
 */
public function has(MonitoredObject $object)
{
    return $object->process_perfdata;
}

/**
 * Create previews
 *
 * This method creates small previews for the monitored object. They are
 * displayed just under object's status. One argument is accepted: the
 * monitored object. It returns HTML code.
 *
 * @return string
 */
public function getPreviewHtml(MonitoredObject $object)
{
    if ($object instanceof Host)
        $url = Url::fromPath(
            'monitoring/host/tabhook',
            [ 'host' => $object->host_name, 'hook' => 'charts' ]
        );
    else if ($object instanceof Service)
        $url = Url::fromPath(
            'monitoring/service/tabhook',
            [
                'host'    => $object->host_name,
                'service' => $object->service,
                'hook'    => 'charts'
            ]
        );
    if (!isset($url))
        return '';

    $prc = new Processor($object);
    $prc->init($object);

    $html = '';
    foreach ($prc->get($prc->defaultProbes) as $probe => $data) {
        $url->overwriteParams([ 'probe' => $probe ]);
        $html .=
            '<a href="' . $url->getRelativeUrl() . '" data-base-target="_self">'
          . '<div class="preview">'
          . '<div class="top">' . htmlspecialchars($probe) . '</div>'
          . '<div class="top">'
          . $prc->formatAge($data->stop_ts - $data->start_ts) . '</div>'
          . '<br>'
          . '<svg width="100%" viewBox="0 0 ' . $prc->graphAspect . ' 100">'
          . '<path class="closed" d="' . $data->path_closed . '"/>'
          . '<path class="open" d="' . $data->path_open . '"/>'
          . '<path class="threshold warning" d="' . $data->path_warn . '"/>'
          . '<path class="threshold critical" d="' . $data->path_crit . '"/>'
          . '</svg>';
        if (isset($data->min))
            $html .=
                '<div class="scale min">'
              . $prc->formatNumber($data->max, $data->unit) . '</div>';
        if (isset($data->max))
            $html .= 
                '<div class="scale max">'
              . $prc->formatNumber($data->min, $data->unit) . '</div>';
        $html .= '</div></a> ';
    }

    return '<br><div class="icinga-module module-charts">' . $html . '</div>';
}

}
