<?php

namespace Icinga\Module\Charts\ProvidedHook;

use Icinga\Module\Charts\Processor;
use Icinga\Module\Charts\Forms\GraphsTabForm;

use Icinga\Exception\ConfigurationError;
use Icinga\Module\Monitoring\Hook\ObjectDetailsTabHook;
use Icinga\Authentication\Auth;
use Icinga\Module\Monitoring\Object\MonitoredObject;
use Icinga\Web\Request;
use Icinga\Web\Url;
use Icinga\Forms\ConfirmRemovalForm;

/**
 * This hook adds an extra tab to the monitored object's view. It contains
 * full scale graphs and a form that allows changing chart parameters. It can
 * also be embedded in a dashboard.
 */
class GraphsTab extends ObjectDetailsTabHook
{

/**
 * Tab identifier
 *
 * Returns the identifier 'charts', which appears in the URL of the tab.
 *
 * @return string
 */
public function getName()
{
    return 'charts';
}

/**
 * Tab display name
 *
 * Returns the string that is displayed to the user.
 *
 * @return string
 */
public function getLabel()
{
    return 'Graphs';
}

/**
 * Check if the tab is available
 *
 * Returns the state of object's process_perfdata variable.
 *
 * @return bool
 */
public function shouldBeShown(MonitoredObject $object, Auth $auth)
{
    return $object->process_perfdata;
}

/**
 * Whether to display host or service status
 *
 * This method controls whether monitored object summary is shown above the
 * content of the tab. Currently it simply returns true.
 *
 * @return bool
 */
public function getHeader(MonitoredObject $object, Request $request)
{
    return true;
}

/**
 * Tab HTML content
 *
 * This method parses URL form data, initializes the Processor object, computes
 * chart horizontal and vertical scales, and generates the final HTML+SVG code.
 *
 * @return string
 */
public function getContent(MonitoredObject $object, Request $request)
{
    $show_compact = $request->getParam('showCompact');
    $show_headers = $request->getParam('showHeaders') ?? 1;

    $probe_filter = $request->getParam('probe');
    if (isset($probe_filter) and strlen($probe_filter))
        $probe_filter = [ $probe_filter ];
    else
        $probe_filter = null;

    $prc = new Processor($object);
    $prc->init($object);

    $start = $prc->parseDateTime(
        $request->getParam('start')
     ?: $request->getParam('startdt') . 'T' . $request->getParam('starttm')
    );
    $stop = $prc->parseDateTime(
        $request->getParam('stop')
     ?: $request->getParam('stopdt') . 'T' . $request->getParam('stoptm')
    );
    $prc->graphStart = $start;
    $prc->graphStop  = $stop;

    $dur = $request->getParam('dur');
    try {
        if ($prc->parseInterval($dur)) {
            $prc->graphLength = $dur;
            if ($start)
                $prc->graphStop = $start + $prc->parseInterval($dur);
        }
    }
    catch (ConfigurationError $e) {
        $dur = null;
    }

    $zero = $request->getParam('zero');
    if (isset($zero))
        $prc->graphZero = $request->getParam('zero');

    $aspect = $request->getParam('aspect');
    if (isset($aspect) and is_numeric($aspect) and $aspect >= .5)
        $prc->graphAspect = $aspect * 100;
    else
        $aspect = null;

    $html = '';

    $refresh = $request->getParam('refresh');
    if ($refresh) {
        try {
            $request->getResponse()->setAutoRefreshInterval(
                $prc->parseInterval($refresh)
            );
        }
        catch (ConfigurationError $e) {
            $refresh = null;
        }
    }
    else if ($show_compact)
        $request->getResponse()->setAutoRefreshInterval(300);

    if (! $show_compact) {
        $form = new GraphsTabForm();
        $form->probes = $prc->getProbes();
        $form->create(
            [
                'startdt' => isset($start) ? date('Y-m-d', $start)    : '',
                'starttm' => isset($start) ? date('H:i', $start) : '',
                'stopdt'  => isset($stop)  ? date('Y-m-d', $stop)     : '',
                'stoptm'  => isset($stop)  ? date('H:i', $stop)  : '',
                'dur'         => $dur,
                'probe'       => $request->getParam('probe') ?? '(all)',
                'zero'        => $prc->graphZero ? true : false,
                'aspect'      => $aspect,
                'refresh'     => $refresh,
                'showHeaders' => $show_headers ? true : false,
            ]
        );
        $html .= '<div class="filter icinga-controls">'
          . preg_replace('/<\\/div>\\n.*?;<\\/span><\\/div>/', '', $form)
          . '</div>';
    }
    if ($show_compact and !$show_headers)
        $html .= '<br>';

    $html .= '<div class="icinga-module module-charts">';

    foreach ($prc->get($probe_filter) as $probe => $data) {
        $hmin = $data->start_ts;
        $hmax = $data->stop_ts;
        $hrange = $hmax - $hmin;
        $hmstep = 1;
        if ($hrange > 47304000) {
            $hstep_unit = 'Y';
            $hmstep = 4838400;
        }
        else if ($hrange > 4838400) {
            $hstep_unit = 'M';
            $hmstep = 86400;
        }
        else if ($hrange > 604800) {
            $hstep_unit = 'w';
            $hmstep = 86400;
        }
        else if ($hrange > 28800) {
            $hstep_unit = 'd';
            $hmstep = 3600;
        }
        else if ($hrange > 3600) {
            $hstep_unit = 'h';
            $hmstep = 60;
        }
        else if ($hrange > 60)
            $hstep_unit = 'm';
        else
            $hstep_unit = 's';
        $htarget = 20 * $prc->graphAspect / 100;

        if ($hmstep < 3600) {
            foreach ([ 2, 5, 10, 15, 30, 60 ] as $d)
                if ($hrange / $hmstep / $d < $htarget) {
                    $hmstep *= $d;
                    break;
                }
        }
        else if ($hmstep < 86400) {
            foreach ([ 2, 3, 4, 6, 12, 24 ] as $d)
                if ($hrange / $hmstep / $d < $htarget) {
                    $hmstep *= $d;
                    break;
                }
        }
        while ($hrange / $hmstep > $htarget)
            $hmstep *= 2;
        if ($hmstep >= 4838400) {
            $hmstep = round($hmstep / 4838400, 0);
            $hmstep_unit = 'M';
        }
        else if ($hmstep >= 604800) {
            $hmstep = round($hmstep / 604800, 0);
            $hmstep_unit = 'w';
        }
        else if ($hmstep >= 86400) {
            $hmstep = round($hmstep / 86400, 0);
            $hmstep_unit = 'd';
        }
        else if ($hmstep >= 3600) {
            $hmstep = round($hmstep / 3600, 0);
            $hmstep_unit = 'h';
        }
        else if ($hmstep >= 60) {
            $hmstep = round($hmstep / 60, 0);
            $hmstep_unit = 'm';
        }
        else
            $hmstep_unit = 's';
        $vmin = $data->min;
        $vmax = $data->max;
        $vrange = $vmax - $vmin;
        $vstep = 1;

        if ($data->unit == 'B' and $vrange > 1) {
            while ($vstep < $vrange / 10)
                $vstep *= 2;
            $vmstep = $vstep;
        }
        else if ($vrange and $vrange < 1) {
            while ($vstep > $vrange)
                $vstep /= 10;
        }
        else
            while ($vstep < $vrange / 10)
                $vstep *= 10;

        if ($vrange / $vstep > 6) {
            $vstep *= 2;
            $vmstep = $vstep / 4;
        }
        else if ($vrange / $vstep < 3) {
            $vstep /= 2;
            $vmstep = $vstep / 5;
        }
        else if ($data->unit == 'B')
            $vmstep = $vstep / 4;
        else
            $vmstep = $vstep / 5;

        if ($vmin < 0)
            for ($vl = 0; $vl > $vmin; $vl -= $vstep);
        else
            for ($vl = 0; $vl < $vmin; $vl += $vstep);
        if ($vmax < 0)
            for ($vh = 0; $vh > $vmax; $vh -= $vstep);
        else
            for ($vh = 0; $vh < $vmax; $vh += $vstep);

        if ($show_headers)
            $html .= '<h2>' . htmlspecialchars($probe) . '</h2>';

        $html .=
            '<div class="chart" style="width: calc(100% - 10ex)";>'
          . '<div class="top">' . date('Y-m-d H:i', $hmin) . '</div>'
          . '<div class="top">' . date('Y-m-d H:i', $hmax) . '</div>'
          . '<svg width="100%" viewBox="0 0 ' . $prc->graphAspect . ' 100">';

        $interval = $data->interval;
        $xmarks = [];
        $ppos = 0;

        $nmdt = $this->dateAdd(
            $this->dateTrunc(getdate($hmin), $hstep_unit),
            $hmstep,
            $hmstep_unit
        );
        while (($nmts = $this->dateTS($nmdt)) < $hmin)
            $nmdt = $this->dateAdd($nmdt, $hmstep, $hmstep_unit);
        $ndt = $this->dateAdd(
            $this->dateTrunc(getdate($hmin), $hstep_unit), 1, $hstep_unit
        );
        $nts = $this->dateTS($ndt);

        $ts  = $hmin;
        $pts = $this->dateTS($this->dateTrunc(getdate($ts), $hstep_unit));
        for ($i = 0; $i < $data->count; $i++, $ts += $interval) {
            if ($ts < $nmts and $ts < $nts)
                continue;
            if ($ts >= $nmts) {
                $nmdt = $this->dateAdd($nmdt, $hmstep, $hmstep_unit);
                $nmts = $this->dateTS($nmdt);
            }
            $x = $prc->graphAspect * $i / $data->count;
            $html .=
                '<path class="grid'
              . ($ts >= $nts ? ' vmajor' : '')
              . '" d="M ' . $x . ' 0'
              . ' L ' . $x . ' 100"/>';
            if ($ts < $nts)
                continue;
            $ndt = $this->dateAdd($ndt, 1, $hstep_unit);
            $nts = $this->dateTS($ndt);
            $pos = 100 * $i / $data->count;
            array_push(
                $xmarks,
                [ 'sp' => $ppos, 'ep' => $pos, 'ts' => $pts ]
            );
            $pts =
                $this->dateTS($this->dateTrunc(getdate($ts), $hstep_unit));
            $ppos = $pos;
        }
        array_push(
            $xmarks,
            [ 'sp' => $ppos, 'ep' => 100, 'ts' => $pts ]
        );

        $dd = $vmstep < 1 ? ceil(-log10($vmstep)) : 0;
        $vp = round($vl, $dd);
        if ($vrange)
            for ($vmp = $vl - $vstep; $vmp < $vh; $vmp += $vmstep) {
                $y = round(100 - 100 * ($vmp - $vmin) / $vrange, 2);
                $major = round($vmp, $dd) == $vp;
                $html .=
                    '<path class="grid'
                  . ( $major ? ' hmajor' : '' )
                  . '" d="M 0 ' . $y
                  . ' L ' . $prc->graphAspect . ' ' . $y
                  . '"/>';
                if ($major)
                    $vp = round($vp + $vstep, $dd);
            }

        $html .= '<path class="closed" d="' . $data->path_closed . '"/>';
        if ($data->zero and $data->zero != 100)
            $html .=
                '<path class="grid zero" d="M 0 ' . $data->zero
              . ' L ' . $prc->graphAspect . ' ' . $data->zero
              . '"/>';
        $html .=
            '<path class="open" d="' . $data->path_open . '"/>'
          . '<path class="threshold warning" d="' . $data->path_warn . '"/>'
          . '<path class="threshold critical" d="' . $data->path_crit . '"/>'
          . '</svg>';

        foreach ($xmarks as $m) {
            if ($m['ep'] - $m['sp'] < 10)
                continue;
            switch ($hstep_unit) {
                case 'Y':
                    $date = date('Y', $m['ts']);
                    break;
                case 'M':
                    $date = date("M Y", $m['ts']);
                    break;
                case 'w':
                    $date = 'week ' . date('W', $m['ts']);
                    break;
                case 'd':
                    $date =
                        date('d', $m['ts'])
                      . ' (' . substr(date('l', $m['ts']), 0, 3) . ')';
                    break;
                case 's':
                    $date = date('H:i:s', $m['ts']);
                    break;
                default:
                    $date = date('H:i', $m['ts']);
            }
            $html .=
                '<div class="xmark" style="left: ' . round($m['sp'], 2)
                . '%; right: ' . round(100 - $m['ep'], 2) . '%;">'
              . $date
              . '</div>';
        }

        $vp = $vl;
        if ($data->unit == 'B')
            for (
                $dd = 0, $sdiv = $vh / $vstep;
                $dd < 3 and $sdiv > 1;
                $dd++, $sdiv /= 10
            );
        else
            for ($dd = 0; $dd < 5; $dd++)
                for (
                    $vp = $vl;
                    $prc->formatNumber($vp, $data->unit, $dd) !=
                    $prc->formatNumber($vp + $vstep, $data->unit, $dd);
                    $vp += $vstep
                  )
                    if ($vp >= $vh)
                        break 2;

        for ($vp = $vl; $vp - $vmin <= $vrange; $vp += $vstep) {
            if ($vp < $vmin)
                continue;
            $html .=
                '<div class="ymark" style="bottom: '
              . round(100 * ($vp - $vmin) / $vrange, 2) . '%;">'
              . $prc->formatNumber($vp, $data->unit, $dd)
              . '</div>';
        }

        $html .= '</div><br><br>';
    }

    return $html . '</div>';
}

/**
 * Helper method that advances date array by some units
 *
 * This method accepts three arguments: an array with date and time returned by
 * getdate(), number of units and unit type: M – months, w – weeks, d – days,
 * h – hours, m – minutes, s – seconds. It advances the desired property by
 * given number of units and returns the result as a new array.
 *
 * @return array
 */
private function dateAdd($dt, $c, $unit)
{
    switch ($unit) {
        case 'Y':
            $dt['year'] += $c;
            return $dt;
        case 'M':
            $dt['mon'] += $c;
            return $dt;
        case 'w':
            $dt['mday'] += $c * 7;
            return $dt;
        case 'd':
            $dt['mday'] += $c;
            return $dt;
        case 'h':
            $dt['hours'] += $c;
            return $dt;
        case 'm':
            $dt['minutes'] += $c;
            return $dt;
        case 's':
            $dt['seconds'] += $c;
            return $dt;
    }
}

/**
 * Helper method that aligns date and time array to specified unit
 *
 * This method accepts two arguments: an array with date and time returned by
 * getdate(), and unit type which is the same as in dateAdd(). It rounds the
 * date down to the given unit and returns the result as a new array.
 *
 * @return array
 */
private function dateTrunc($dt, $unit)
{
    switch ($unit) {
        case 'Y':
            $dt['hours'] = $dt['minutes'] = $dt['seconds'] = 0;
            $dt['mon'] = $dt['mday'] = 1;
            return $dt;
        case 'M':
            $dt['hours'] = $dt['minutes'] = $dt['seconds'] = 0;
            $dt['mday'] = 1;
            return $dt;
        case 'w':
            $dt['hours'] = $dt['minutes'] = $dt['seconds'] = 0;
            $ndt = $dt;
            while ( $ndt['wday'] != 1 ) {
                $ndt['mday']--;
                $ndt = getdate($this->dateTS($ndt));
            }
            $dt['mday'] = $ndt['mday'];
            $dt['mon']  = $ndt['mon'];
            $dt['year'] = $ndt['year'];
            return $dt;
        case 'd':
            $dt['hours'] = $dt['minutes'] = $dt['seconds'] = 0;
            return $dt;
        case 'h':
            $dt['minutes'] = $dt['seconds'] = 0;
            return $dt;
        case 'm':
            $dt['seconds'] = 0;
            return $dt;
        case 's':
            return $dt;
    }
}

/**
 * Helper method that creates timestamp from date and time
 *
 * This method accepts a date and time array and converts it to timestamp.
 *
 * @return int
 */
private function dateTS ($dt)
{
    return mktime(
        $dt['hours'], $dt['minutes'], $dt['seconds'],
        $dt['mon'],   $dt['mday'],    $dt['year']
    );
}

}
