<?php

$this->provideConfigTab(
    'config',
    array (
        'title' => $this->translate('Main settings'),
        'label' => $this->translate('General'),
        'url'   => 'config'
    )
);

$this->provideCssFile('charts.less');
