<?php

namespace Icinga\Module\Charts\Controllers;

use Icinga\Web\Controller;
use Icinga\Module\Charts\Forms\Config\GeneralConfigForm;

class ConfigController extends Controller
{

public function indexAction()
{
    $this->assertPermission('config/modules');
    $form = new GeneralConfigForm();
    $form->setIniConfig($this->Config());
    $form->handleRequest();
    $this->view->form = $form;
    $this->view->tabs =
        $this->Module()->getConfigTabs()->activate('config');
}

}
