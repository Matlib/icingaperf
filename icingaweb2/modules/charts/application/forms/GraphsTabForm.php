<?php

namespace Icinga\Module\Charts\Forms;

use Icinga\Web\Form;

class GraphsTabForm extends Form
{

public $probes;

public function init()
{
    $this->setAction($this->getView()->url()->getRelativeUrl());
    $this->setMethod('GET');
    $this->setSubmitLabel($this->translate('Apply'));
}

public function createElements(array $formData)
{
    $this->addElement(
        'date',
        'startdt',
        [
            'label' => $this->translate('Start'),
            'value' => $formData['startdt'],
        ],  
    );
    $this->addElement(
        'time',
        'starttm',
        [ 'value' => $formData['starttm'] ],
    );
    $this->addElement(
        'date',
        'stopdt',
        [
            'label' => $this->translate('Stop'),
            'value' => $formData['stopdt'],
        ],
    );
    $this->addElement(
        'time',
        'stoptm',
        [ 'value' => $formData['stoptm'] ],
    );

    $this->addElement(
        'text',
        'dur',
        [
            'label'       => $this->translate('Duration'),
            'value'       => $formData['dur'],
            'description' => $this->translate(
                'Graph time span. Numbers may be suffixed with d (for days), '
              . 'h (hours), or m (minutes) in that order. Bare number '
              . 'specifies seconds.'
            ),
        ],
    );

    $opt = [ '' => '(all)' ];
    foreach ($this->probes as $p)
        $opt[$p] = $p;
    $this->addElement(
        'select',
        'probe',
        [
            'label'        => $this->translate('Performance data'),
            'multiOptions' => $opt,
            'value'        => $formData['probe'],
        ],
    );

    $this->addElement(
        'checkbox',
        'zero',
        [
            'label'       => $this->translate('Zero'),
            'value'       => $formData['zero'],
            'description' => $this->translate(
                'Whether the graph\'s vertical axis should always include the '
              . 'zero point. Normally the scale is automatically adjusted '
              . 'between the visible minimum and maximum.'
            ),
        ],
    );

    $this->addElement(
        'text',
        'aspect',
        [
            'label' => $this->translate('Aspect ratio'),
            'value' => $formData['aspect'],
        ],
    );

    $this->addElement(
        'text',
        'refresh',
        [
            'label'       => $this->translate('Autorefresh'),
            'value'       => $formData['refresh'],
            'description' => $this->translate(
                'Tab refresh interval. The number format is the same as '
              . 'Duration above.'
            ),
        ],
    );

    $this->addElement(
        'checkbox',
        'showHeaders',
        [
            'label' => $this->translate('Show graph titles'),
            'value' => $formData['showHeaders'],
        ],
    );

    $this->addDisplayGroup(
        [ 'dur', 'probe', 'zero', 'aspect', 'refresh', 'showHeaders' ],
        'advanced_options',
        [
            'decorators' => [
                'FormElements',
                [
                    'Fieldset',
                    [
                        'class'               => 'collapsible',
                        'data-visible-height' => 0,
                    ],
                ],
            ],
        ]
    );
}

}
