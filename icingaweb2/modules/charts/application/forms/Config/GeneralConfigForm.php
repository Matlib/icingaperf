<?php

namespace Icinga\Module\Charts\Forms\Config;

use Icinga\Data\ResourceFactory;
use Icinga\Forms\ConfigForm;

class GeneralConfigForm extends ConfigForm
{

public function init()
{
    $this->setName('form_config_charts');
    $this->setSubmitLabel($this->translate('Save Changes'));
}

public function createElements(array $formData)
{
    $resources = [];
    foreach (ResourceFactory::getResourceConfigs() as $name => $resource)
        if ($resource->type == 'db')
            $resources[$name] = $name;

    if (empty($resources))
        throw new ConfigurationError(
            $this->translate('There are no configured IDO resources.')
        );

    $this->addElement(
        'select',
        'backend_ido',
        [
            'required'    => true,
            'label'       => $this->translate('IDO Backend'),
            'description' =>
                $this->translate('IDO data source to use for plotting'),
            'multiOptions' => $resources,
            'autosubmit'   => true
        ]
    );
    $selected =
        isset($formData['backend_ido'])
      ? $formData['backend_ido']
      : $this->getValue('backend_ido');
    if ($selected)
        $this->addElement(
            'note',
            'resource_note',
            [
                'escape'        => false,
                'value'         =>
                    '<a href="'
                  . $this->getView()->url('config/editresource',
                    array('resource' => $selected))
                  . '" data-base-target="_next">'
                  . $this->translate('Show resource configuration')
                  . '</a>',
            ]
        );

    $this->addElement(
        'text',
        'graph_length',
        [
            'value'       => '6h',
            'label'       => $this->translate('Default graph length'),
            'description' => $this->translate(
                'The default time span that is covered by graphs'
            )
        ]
    );
    $this->addElement(
        'number',
        'graph_points',
        [
            'value'       => '1000',
            'label'       =>
                $this->translate('Maximum number of data points'),
            'description' => $this->translate(
                  'The expected maximum number of data points '
                . 'that are displayed before averaging is done'
            )
        ]
    );
    $this->addElement(
        'text',
        'graph_overlap',
        [
            'value'       => '10s',
            'label'       => $this->translate('Data overlapping'),
            'description' => $this->translate(
                  'The amount of time that is added to the boundaries '
                . 'of the sampling period to avoid interrupting the plot '
                . 'due to irregular probe timestamps'
            )
        ]
    );
    $this->addElement(
        'number',
        'graph_probespan',
        [
            'value'       => '3',
            'label'       => $this->translate('Probe search range'),
            'description' => $this->translate(
                  'Number of days before the start date when probes are being '
                . 'looked for. Any probes older than that expire and are not '
                . 'shown. Zero disables this functionality.'
            )
        ]
    );
    $this->addElement(
        'text',
        'graph_aspect',
        [
            'value'       => '2.5',
            'label'       => $this->translate('Graph aspect ratio'),
            'description' => $this->translate(
                  'Performance graph width to height aspect ratio'
            )
        ]
    );

    $this->addElement(
        'checkbox',
        'graph_zero',
        [
            'label'       => $this->translate('Always include zero'),
            'description' => $this->translate(
                'Always show the zero point in graphs'
            )
        ]
    );
}

}
